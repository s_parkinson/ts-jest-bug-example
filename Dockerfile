FROM node:13.6.0

ADD . /app

RUN cd /app/with-dependency && npm install
RUN cd /app/with-dependency && npm run test