import { fn } from '../index'

describe('simple test', () => {
  it('fn returns correctly', async () => {
    expect(fn()).toEqual(1.0)
  })
})
